#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#define PI (3.141592653589793)

int Calculate(GObject *Zelf, gpointer builder)
{
    GObject *OrbitHeightentry;
    GObject *Amountentry;
    GObject *DropDown;
    GObject *ApLabel;
    GObject *PeLabel;
    gchar *SOI;
    const gchar *OrbitHeightp;
    const gchar *Amountp;
    char *pEnd;
    char *pEnd2;
    OrbitHeightentry = gtk_builder_get_object(GTK_BUILDER(builder), "Orbit");
    Amountentry = gtk_builder_get_object(GTK_BUILDER(builder), "Amount");
    DropDown = gtk_builder_get_object(GTK_BUILDER(builder), "Planet (SOI) Selector");
    ApLabel = gtk_builder_get_object(GTK_BUILDER(builder), "Apoapsis");
    PeLabel = gtk_builder_get_object(GTK_BUILDER(builder), "Periapsis");

    OrbitHeightp = gtk_entry_get_text(GTK_ENTRY(OrbitHeightentry));
    int OrbitHeight = strtol(OrbitHeightp,&pEnd,10);
    Amountp = gtk_entry_get_text(GTK_ENTRY(Amountentry));
    int Amount = strtol(Amountp,&pEnd2,10);
    SOI = gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(DropDown));
    double GM = 0;
    double SH = 0;
    char Kerbol[] = "Kerbol", Moho[] = "Moho", Eve[] = "Eve", Gilly[] = "Gilly", Kerbin[] = "Kerbin", Mun[] = "Mun", Minmus[] = "Minmus", Duna[] = "Duna", Ike[] = "Ike", Dres[] = "Dres", Jool[] = "Jool", Laythe[] = "Laythe", Vall[] = "Vall", Tylo[] = "Tylo", Bop[] = "Bop", Pol[] = "Pol", Eeloo[] = "Eeloo";

    if (strcmp(SOI, Kerbol) == 0) {
        GM = 1.1723328*(pow(10, 18));
        SH = 261600000;
    }
    else if (strcmp(SOI, Moho) == 0) {
        GM = 1.6860938*(pow(10, 11));
        SH = 600000;
    }
    else if (strcmp(SOI, Eve) == 0) {
        GM = 8.1717302*(pow(10, 12));
        SH = 700000;
    }
    else if (strcmp(SOI, Gilly) == 0) {
        GM = 8289449.8;
        SH = 13000;
    }
    else if (strcmp(SOI, Kerbin) == 0) {
        GM = 3.5316*(pow(10, 12));
        SH = 600000;
    }
    else if (strcmp(SOI, Mun) == 0) {
        GM = 6.5138398*(pow(10, 10));
        SH = 200000;
    }
    else if (strcmp(SOI, Minmus) == 0) {
        GM = 1.7658000*(pow(10, 9));
        SH = 60000;
    }
    else if (strcmp(SOI, Duna) == 0) {
        GM = 3.0136321*(pow(10, 11));
        SH = 320000;
    }
    else if (strcmp(SOI, Ike) == 0) {
        GM = 1.8568369*(pow(10, 10));
        SH = 130000;
    }
    else if (strcmp(SOI, Dres) == 0) {
        GM = 2.1484489*(pow(10, 10));
        SH = 138000;
    }
    else if (strcmp(SOI, Jool) == 0) {
        GM = 2.8252800*(pow(10, 14));
        SH = 6000000;
    }
    else if (strcmp(SOI, Laythe) == 0) {
        GM = 1.9620000*(pow(10, 12));
        SH = 500000;
    }
    else if (strcmp(SOI, Vall) == 0) {
        GM = 2.0748150*(pow(10, 11));
        SH = 300000;
    }
    else if (strcmp(SOI, Tylo) == 0) {
        GM = 2.8252800*(pow(10, 12));
        SH = 600000;
    }
    else if (strcmp(SOI, Bop) == 0) {
        GM = 2.4868349*(pow(10, 9));
        SH = 65000;
    }
    else if (strcmp(SOI, Pol) == 0) {
        GM = 7.2170208*(pow(10, 8));
        SH = 44000;
    }
    else if (strcmp(SOI, Eeloo) == 0) {
        GM = 7.4410815*(pow(10, 10));
        SH = 210000;
    }
    
    double SemiMajorAxis = OrbitHeight+SH;
    double OrbitalPeriod = 2*PI*sqrtf(pow(SemiMajorAxis, 3)/GM);
    double NeededPeriod = Amount;
    NeededPeriod = ((NeededPeriod-1)/NeededPeriod)*OrbitalPeriod;
    double TotalAxis = 2*cbrtf((GM*pow(NeededPeriod, 2))/(4*pow(PI, 2)));
    double Apoapsis = OrbitHeight;
    double Periapsis = TotalAxis-(2*SH)-Apoapsis;
    char Apo[64];
    sprintf(Apo, "%lf", Apoapsis);
    char Per[64];
    sprintf(Per, "%lf", Periapsis);

    gtk_label_set_text(GTK_LABEL(ApLabel), Apo);
    gtk_label_set_text(GTK_LABEL(PeLabel), Per);

    g_free(SOI);

    return 0;
}

int main(int argc, char *argv[])
{
    GtkBuilder      *builder; 
    GtkWidget       *window;
    GObject         *CalculateButton;

    gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "builder.ui", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));

    CalculateButton = gtk_builder_get_object(builder, "Calculate");
    g_signal_connect(CalculateButton, "clicked", G_CALLBACK(Calculate), builder);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    gtk_widget_show(window);
    gtk_main();

    return 0;
}
